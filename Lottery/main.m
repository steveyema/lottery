//
//  main.m
//  Lottery
//
//  Created by Ye Ma on 2016-11-21.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

NSUInteger const LOTTERY_NUMBER_MIN = 1;
NSUInteger const LOTTERY_NUMBER_MAX = 59;
NSUInteger const LOTTERY_NUMBER_COUNT = 7;

@interface LotteryNumberGenerator : NSObject

@end

@implementation LotteryNumberGenerator

+(NSArray *)lotteriesFromInputs:(NSArray *)inputs
{
    NSMutableArray *results = [[NSMutableArray alloc] initWithCapacity:inputs.count];
    for (NSString *input in inputs)
    {
        NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:1];
        [LotteryNumberGenerator lotteryNumbersWithInputString:input withNumberOfDigit:1 fromIndex:0 withBuffer:[[NSMutableArray alloc] initWithCapacity:LOTTERY_NUMBER_COUNT] andResult:result];
        [LotteryNumberGenerator lotteryNumbersWithInputString:input withNumberOfDigit:2 fromIndex:0 withBuffer:[[NSMutableArray alloc] initWithCapacity:LOTTERY_NUMBER_COUNT] andResult:result];
        
        [results addObjectsFromArray:result];
    }
    return results;
}

+(void)lotteryNumbersWithInputString:(NSString *)inputString withNumberOfDigit:(NSInteger)numberOfDigit fromIndex:(NSUInteger)index withBuffer:(NSMutableArray *)buffer andResult:(NSMutableArray *)result
{
    if (index + numberOfDigit > inputString.length)
    {
        return;
    }
    
    NSString *pickedString = [inputString substringWithRange:NSMakeRange(index, numberOfDigit)];
    NSInteger pickerNumber = [pickedString integerValue];
    
    if (pickerNumber < LOTTERY_NUMBER_MIN || pickerNumber > LOTTERY_NUMBER_MAX)
    {
        return;
    }
    
    [buffer addObject:@(pickerNumber)];
    index += numberOfDigit;
    
    if (buffer.count == LOTTERY_NUMBER_COUNT)
    {
        if (index ==  inputString.length)
        {
            [result addObject:buffer];
        }
        
        return;
    }
    
    NSMutableArray *buffer1 = [buffer mutableCopy];
    NSMutableArray *buffer2 = [buffer mutableCopy];
    
    [LotteryNumberGenerator lotteryNumbersWithInputString:inputString withNumberOfDigit:1 fromIndex:index withBuffer:buffer1 andResult:result];
    [LotteryNumberGenerator lotteryNumbersWithInputString:inputString withNumberOfDigit:2 fromIndex:index withBuffer:buffer2 andResult:result];
    
}

@end

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        
        NSArray *results = [LotteryNumberGenerator lotteriesFromInputs:@[@"1",@"42",@"4938532894754",@"1234567",@"472844278465445"]];
        NSLog(@"%@", results);
    }
    return 0;
}
